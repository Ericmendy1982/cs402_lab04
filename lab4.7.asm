.data 
prompt: .asciiz "Please type in two non-negative integers:"
output: .asciiz "The result is:"
re_enter: .asciiz "Not all numbers are non-negative, u need to re-enter:"

.text
.globl main

main:
     li $v0, 4
     la $a0, prompt
     syscall
     li $v0, 5        # read the first integer
     syscall
     move $t0,$v0
     li $v0, 5        # read the second integer
     syscall
     move $t1,$v0
      
     bltz $t0, re_type      # make sure the 2 numbers are non-negative
     bltz $t1, re_type
     j next

re_type:
     li $v0, 4
     la $a0, re_enter
     syscall
     j main 

next:
     li $v0,4              
     la $a0, output                  
     syscall
     
     sub $sp,$sp,4                     # save returning address
     sw $ra,4($sp)
     move $a0,$t0                      # pass parameters 
     move $a1,$t1
     jal Ackermann                     # call procedure Ackermann
     move $a0, $v0                 
     li $v0,1
     syscall                       # print the result
     lw $ra, 4($sp)                # restore $ra
     addi $sp,$sp,4
     jr $ra
     
Ackermann:
     beq $a0,$0, Xequals0               # termination condition, when x=0
     sub $sp,$sp,4                 # save $ra
     sw  $ra,4($sp)
     beq $a1,$0, Yequals0               # branch condition, when y=0 , A(x-1,1) 
     sub $t0,$a0,1                
     sub $sp,$sp,4
     sw $t0,4($sp)                      # save x-1 in stack
     addi $a1,$a1,-1
     jal Ackermann                 # call for A(x, y-1) 
     lw $a0,4($sp)
     addi $sp,$sp,4                # A(x-1,A(x,y-1))
     move $a1,$v0 
     jal Ackermann
     j Exit

Xequals0: 
     addi $v0,$a1,1
     jr $ra

Yequals0:
     addi $a0,$a0,-1               # A(x-1,1) 
     li $a1,1
     jal Ackermann

Exit:
     lw $ra, 4($sp)
     addi $sp,$sp,4
     jr $ra
     
     
     
     
     
     
     