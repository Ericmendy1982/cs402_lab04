         .data
prompt:  .asciiz "Please enter an integer:"        
message: .asciiz "The larger value is:"   
      
         .text
         .globl main
main:    add $sp,$sp,-4       # return address of main  
         sw $ra,4($sp)
         li $v0,4             # input and save first integer
         la $a0, prompt
         syscall
         li $v0, 5
         syscall
         addu $t0,$v0,$0
         li $v0,4             # input and save second integer
         la $a0, prompt
         syscall
         li $v0, 5
         syscall
         addu $t1,$v0,$0
         jal Largest
         nop
         lw $ra,4($sp)
         add $sp, $sp,4
         jr $ra


Largest:  slt $t2, $t0,$t1          # compare 2 integers
          beq $t2,$0, label1
          li $v0,4
          la $a0, message
          syscall
          li $v0,1                  # if the second integer is bigger, output
          la $a0,($t1)              # the second integer
          syscall
          jr $ra
          

label1:   li $v0,4
          la $a0, message           # if the first integer is bigger, output 
          syscall                   # the first integer
          li $v0,1
          la $a0,($t0)
          syscall
          jr $ra
          
          
