         .data
prompt:  .asciiz "Please enter an integer:"        
message: .asciiz "The larger value is:"   
      
         .text
         .globl main
main:    add $sp,$sp,-12       # return address of main  
         sw $ra,4($sp)
         li $v0,4             # input and save first integer
         la $a0, prompt
         syscall
         li $v0, 5
         syscall
         sw $v0,8($sp)
         li $v0,4             # input and save second integer
         la $a0, prompt
         syscall
         li $v0, 5
         syscall
         sw $v0,12($sp)
         jal Largest
         nop
         lw $ra,4($sp)
         add $sp, $sp,12
         jr $ra
         

Largest:  lw $t0,8($sp)              # compare the 2 integers
          lw $t1,12($sp)
          slt $t2, $t0,$t1
          beq $t2,$0, label1
          li $v0,4                    # if the second integer is bigger, output
          la $a0, message             # the second integer
          syscall
          li $v0,1
          la $a0,($t1)
          syscall
          jr $ra
          

label1:   li $v0,4                  # if first integer is bigger, output the first
          la $a0, message           # integer
          syscall
          li $v0,1
          la $a0,($t0)
          syscall
          jr $ra
          
          
