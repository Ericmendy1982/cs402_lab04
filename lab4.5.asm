         .data
prompt:  .asciiz "Please enter an integer:"        
message1: .asciiz "The factorial is:"   
message2: .asciiz "The number is negative, please re_enter:"

      
         .text
         .globl main
main:    
         li $v0,4             # input an integer
         la $a0, prompt
         syscall
  
         li $v0, 5
         syscall
         
  bltz $t0, ReInput     # if the number is negative, re-input    
  addu $a0,$v0,$0
  sub $sp,$sp,4
  sw $ra,4($sp)

  jal Factorial

  lw $ra,4($sp)
  add $sp,$sp,4
  move $t0,$v0

   li $v0,4               # output the factorial value
   la $a0,message1
    syscall

   li $v0,1
   move $a0,$t0
   syscall	
   jr $ra   
       
         
ReInput: li $v0,4
          la $a0, message2
          syscall
          li $v0,5 
          syscall
         
Factorial:
         subu $sp,$sp,4       # save the return address on stack
         sw $ra,4($sp)
         
         beqz $a0, terminate    # termination condition
         subu $sp,$sp,4
         sw $a0,4($sp)          # push all num-1 into the stack
         sub $a0,$a0,1
         jal Factorial
          
         lw $t0,4($sp)          # pop numbers from the stack to make multiplication
         mul $v0, $v0,$t0
         lw $ra, 8($sp)
         addu $sp,$sp,8
         
         jr $ra
         

terminate:
         li $v0,1
         lw $ra,4($sp)
         addu $sp,$sp,4
         jr $ra
          


          
          
